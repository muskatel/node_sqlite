const sqlite3 = require('sqlite3')

const morgan = require('morgan')

const express = require("express");
const bodyParser = require("body-parser");
const port = process.env.PORT || 8080;
const app = express();

// Database from file                                                                                                                        
const db = new sqlite3.Database(__dirname+'/database.sqlite') // Created if it does not exist
// OR in memory only
//const db = new sqlite3.Database(':memory:');
//db.run("CREATE TABLE webusers (username TEXT)");


app.use(morgan('dev'))

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({extended: true}))

app.get("/", (req, res) => {
  	res.send("Hello");
});

const CREATE_WEBUSERS = "CREATE TABLE if not exists webusers (userID INTEGER PRIMARY KEY AUTOINCREMENT, username TEXT, email TEXT);"
const DROP_WEBUSERS = "DROP TABLE if exists webusers;"

app.get("/create", (req, res) => {
	db.run(CREATE_WEBUSERS)
	res.send("Table created");
});

app.get("/drop", (req, res) => {
	db.run(DROP_WEBUSERS)
	res.send("Table dropped");
});

app.get("/reset", (req, res) => {
	db.run(DROP_WEBUSERS, () => {
		console.log("Table dropped ...")
		db.run(CREATE_WEBUSERS, () => { 
			console.log("...  and re-created")

			db.run("INSERT INTO webusers (username, email) VALUES (\'Craig\',  \'craig.marais@witted.com\');")
			db.run("INSERT INTO webusers (username, email) VALUES (\'JC\',   \'JC@gmail.com\');")
			db.run("INSERT INTO webusers (username, email) VALUES (\'Greg\',  \'greg@notcraig.com\');")
		})
	})
	
	res.send("Table reset (dropped and re-created)")
});

app.get("/show", (req, res) => {
	let data = []
	db.serialize(() => {
		db.each("SELECT * FROM webusers;", (err,row) =>
		{
			console.log(row.username)
			data.push(row)
		}, 
		() => { res.send(data) })
	})	
})

//use postman
app.get("/user", (req, res) => {
	console.log(req.body)

	let user = req.body.username
	let data = []
	db.serialize(() => {
		db.each("SELECT * FROM webusers WHERE username=\'"+user+"\';", (err,row) =>
		{
			console.log(row.username)
			data.push(row)
		}, 
		() => { res.send(data)})
	})	
})

//use postman
app.post("/user",(req, res) => {
	console.log(req.body)

	let user = req.body.username
	let email = req.body.email

	db.run("INSERT INTO webusers (username, email) VALUES (\'"+user+"\',  \'"+email+"\');")
	res.send("Saved")

	})

app.listen(port, () =>
  	console.log('Server is running on port:', port)
);